package com.example.game.util;

public class Constantes {
    public static final int COLUMNAS=8;
    public static final int FILAS =8;
    public static final int VACIA =0;
    public static final int BLANCA =1;
    public static final int NEGRA =2;
    public static final int POSIBLE=3;
    public static final int HORIZONTAL_DERECHA=4;
    public static final int HORIZONTAL_IZQUIERDA=5;
    public static final int VERTICAL_ARRIBA=6;
    public static final int VERTICAL_ABAJO=9;
    public static final int DIAGONAL_IZQUIERDA_ARRIBA=10;
    public static final int DIAGONAL_IZQUIERDA_ABAJO=11;
    public static final int DIAGONAL_DERECHA_ARRIBA=12;
    public static final int DIAGONAL_DERECHA_ABAJO=13;
}
