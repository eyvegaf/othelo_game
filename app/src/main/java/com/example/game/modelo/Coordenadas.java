package com.example.game.modelo;

public class Coordenadas {
    private int fila;
    private int col;

    public int getFila() {
        return fila;
    }

    public int getCol() {
        return col;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public Coordenadas(){ }
    public Coordenadas(int fila, int col){
        this.fila=fila;
        this.col=col;
    }
}
