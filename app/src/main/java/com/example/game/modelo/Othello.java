package com.example.game.modelo;

import com.example.game.controlador.Control_touch;
import com.example.game.juego.MainActivity;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import android.content.Context;

public class Othello implements Observer {
    private Actualizar_juego actualizarjuego;
    private Context contexto;
    private Control_touch control_touch;
    public Othello(MainActivity mainActivity, Context contexto){
        this.actualizarjuego =new Actualizar_juego(mainActivity, contexto);
        this.contexto = contexto;
        control_touch = new Control_touch(actualizarjuego.getCasilla());
        control_touch.addObserver(this);
    }


    @Override
    public void update(Observable observable, Object o) {
        ArrayList<Object> elemento = (ArrayList<Object>) o;

        Coordenadas coordenadas=(Coordenadas)elemento.get(0);
        int y=coordenadas.getFila();
        int x=coordenadas.getCol();
        this.actualizarjuego.jugada_valida(y,x);

    }
}
