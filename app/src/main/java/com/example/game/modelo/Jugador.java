package com.example.game.modelo;

public class Jugador {
    private int id;
    private String usuario;
    private String color_ficha;
    private int score;

    public Jugador(){ }

    public Jugador(int id, String usuario, String color_ficha, int score) {
        this.id = id;
        this.usuario = usuario;
        this.color_ficha = color_ficha;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getColor_ficha() {
        return color_ficha;
    }

    public void setColor_ficha(String color_ficha) {
        this.color_ficha = color_ficha;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
