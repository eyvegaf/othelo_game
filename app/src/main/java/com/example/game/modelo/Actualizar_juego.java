package com.example.game.modelo;

import android.graphics.Point;
import androidx.gridlayout.widget.GridLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import android.content.Context;
import android.view.Display;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.game.util.Constantes;
import com.example.game.juego.MainActivity;
import com.example.game.R;

public class Actualizar_juego {

    private int nBlancas;
    private int nNegras;
    private int direccion_jugada;
    private int [][] board;
    private int turno=Constantes.NEGRA;
    private ConstraintLayout casilla[][];
    private TextView textView_blancas;
    private TextView textView_negras;
    private GridLayout matriz;
    private Constantes constantes;
    private Logica logica;
    private Context contexto;
    private MainActivity juego;

    public ConstraintLayout[][] getCasilla() {
        return casilla;
    }


    public Actualizar_juego(MainActivity juego, Context contexto) {
        this.juego = juego;
        this.nBlancas=2;
        this.nNegras=2;
        this.logica = new Logica();
        this.contexto=contexto;
        this.textView_blancas=juego.findViewById(R.id.puntuacion_blancas);
        this.textView_negras=juego.findViewById(R.id.puntuacion_negras);
        this.board = new int[Constantes.FILAS][Constantes.COLUMNAS];
        this.matriz=juego.findViewById(R.id.matriz);
        Point size = new Point();
        Display display = juego.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        this.casilla = new ConstraintLayout[Constantes.FILAS][Constantes.COLUMNAS];
        matriz.setRowCount(Constantes.FILAS);
        matriz.setColumnCount(Constantes.COLUMNAS);
        for (int i = 0; i < Constantes.FILAS; i++){
            for (int j = 0; j < Constantes.COLUMNAS; j++){
                casilla[i][j] = new ConstraintLayout(juego);
                casilla[i][j].setBackgroundResource(R.drawable.fondo);
                if((i==3 && j==3) ||  (i==4 && j==4)){
                    ImageView fblanca = new ImageView(this.contexto);
                    fblanca.setBackgroundResource(R.drawable.fblanca);
                    fblanca.setMaxHeight(casilla[0][0].getMaxHeight());
                    fblanca.setMinimumHeight(casilla[0][0].getMaxHeight());
                    fblanca.setMinimumWidth(casilla[0][0].getMinWidth());
                    fblanca.setMaxWidth(casilla[0][0].getMaxWidth());
                    casilla[i][j].addView(fblanca);
                    this.board[i][j]=Constantes.BLANCA;

                }else if((i==3 && j==4) || (i==4 && j==3)){
                    ImageView fnegra = new ImageView(contexto);
                    fnegra.setBackgroundResource(R.drawable.fazul);
                    fnegra.setMaxHeight(casilla[0][0].getMaxHeight());
                    fnegra.setMinimumHeight(casilla[0][0].getMaxHeight());
                    fnegra.setMinimumWidth(casilla[0][0].getMinWidth());
                    fnegra.setMaxWidth(casilla[0][0].getMaxWidth());
                    casilla[i][j].addView(fnegra);
                    this.board[i][j]=Constantes.NEGRA;
                }else{
                    this.board[i][j]=Constantes.VACIA;
                }
                casilla[i][j].setMinHeight((int) (height * 0.65 / Constantes.FILAS));
                casilla[i][j].setMinWidth((int) (width * 0.90 / Constantes.COLUMNAS));
                casilla[i][j].setMaxHeight((int) (height * 0.65 / Constantes.FILAS));
                casilla[i][j].setMaxWidth((int) (width * 0.90 / Constantes.COLUMNAS));
                matriz.addView(casilla[i][j]);
            }
        }

        iniciar_juego();
       textView_blancas.setText(String.valueOf(nBlancas));
       textView_negras.setText(String.valueOf(nNegras));
    }

    public Actualizar_juego(int nBlancas, int nNegras, int[][] board, boolean turno_fnegra) {
        this.nBlancas = nBlancas;
        this.nNegras = nNegras;
        this.board = board;
    }
    public void iniciar_juego(){
        movimiento_posible();
    }

    public void movimiento_posible(){
        int oponente;
        if (turno== Constantes.BLANCA){
            oponente = Constantes.NEGRA;
        }else  oponente = Constantes.BLANCA;

        for (int i = 0; i < Constantes.FILAS; i++) {
            for (int j = 0; j < Constantes.COLUMNAS; j++) {
                //-----------TURNO PARA LAS NEGRAS Y BLANCAS-----------//
                if(board[i][j]==oponente) {

                    //--------- validando movimiento: diagonal_derecha_abajo ---------//
                    if (i > 0 && j > 0) {
                        if ((board[i - 1][j - 1] == Constantes.VACIA) && (board[i - 1][0] >= 0 && board[0][j - 1] >= 0)) {
                            if (this.logica.diagonal_derecha_abajo(i, j, this.turno, this.board) == 1) {//mov diagonal derecha, abajo es posible
                                ImageView posible = new ImageView(contexto);
                                posible.setBackgroundResource(R.drawable.moviento_posible);
                                posible.setMaxHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumWidth(casilla[0][0].getMinWidth());
                                posible.setMaxWidth(casilla[0][0].getMaxWidth());
                                casilla[i - 1][j - 1].addView(posible);
                                board[i - 1][j - 1] = Constantes.POSIBLE;
                            }
                        }
                    }
                    //--------- validando movimiento: diagonal_derecha_arriba ---------//
                    if (i < Constantes.FILAS - 1 && j > 0) {
                        if ((board[i + 1][j - 1] == Constantes.VACIA) && (board[i + 1][0] < Constantes.FILAS && board[0][j - 1] >= 0)) {
                            if (this.logica.diagonal_derecha_arriba(i, j, this.turno, board) == 1) {
                                ImageView posible = new ImageView(contexto);
                                posible.setBackgroundResource(R.drawable.moviento_posible);
                                posible.setMaxHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumWidth(casilla[0][0].getMinWidth());
                                posible.setMaxWidth(casilla[0][0].getMaxWidth());
                                casilla[i + 1][j - 1].addView(posible);
                                board[i + 1][j - 1] = Constantes.POSIBLE;
                            }
                        }
                    }
                    //--------- validando movimiento: diagonal_izquierda_arriba---------//
                    if (i < Constantes.FILAS - 1 && j < Constantes.COLUMNAS - 1) {
                        if ((board[i + 1][j + 1] == Constantes.VACIA) && (board[i + 1][0] < Constantes.FILAS && board[0][j + 1] < Constantes.COLUMNAS)) {
                            if (this.logica.diagonal_izquierda_arriba(i, j, this.turno, this.board) == 1) {
                                ImageView posible = new ImageView(contexto);
                                posible.setBackgroundResource(R.drawable.moviento_posible);
                                posible.setMaxHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumWidth(casilla[0][0].getMinWidth());
                                posible.setMaxWidth(casilla[0][0].getMaxWidth());
                                casilla[i + 1][j + 1].addView(posible);
                                board[i + 1][j + 1] = Constantes.POSIBLE;
                            }
                        }
                    }
                    //--------- validando movimiento: diagonal_izquierda_abajo---------//
                    if (i > 0 && j < Constantes.COLUMNAS - 1) {
                        if ((board[i - 1][j + 1] == Constantes.VACIA) && (board[i - 1][0] >= 0 && board[0][j + 1] < Constantes.COLUMNAS)) {
                            if (this.logica.diagonal_izquierda_abajo(i, j, this.turno, this.board) == 1) {
                                ImageView posible = new ImageView(contexto);
                                posible.setBackgroundResource(R.drawable.moviento_posible);
                                posible.setMaxHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumWidth(casilla[0][0].getMinWidth());
                                posible.setMaxWidth(casilla[0][0].getMaxWidth());
                                casilla[i - 1][j + 1].addView(posible);
                                board[i - 1][j + 1] = Constantes.POSIBLE;
                            }
                        }
                    }
                    //--------- validando movimiento: horizontal_derecha ---------//
                    if (j > 0 && j < Constantes.COLUMNAS - 1) {
                        if ((board[i][j - 1] == Constantes.VACIA) && (board[0][j - 1] >= 0) && (board[0][j + 1] < Constantes.COLUMNAS)) {
                            if (this.logica.horizontal_derecha(i, j, this.turno, this.board) == 1) {
                                ImageView posible = new ImageView(contexto);
                                posible.setBackgroundResource(R.drawable.moviento_posible);
                                posible.setMaxHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumWidth(casilla[0][0].getMinWidth());
                                posible.setMaxWidth(casilla[0][0].getMaxWidth());
                                casilla[i][j - 1].addView(posible);
                                board[i][j - 1] = Constantes.POSIBLE;
                            }
                        }
                    }
                    //--------- validando movimiento: horizontal_izquierda ---------//
                    if (j < Constantes.COLUMNAS - 1 && j>0) {
                        if ((board[i][j + 1] == Constantes.VACIA) && (board[0][j - 1] >= 0) && (board[0][j + 1] < Constantes.COLUMNAS)) {
                            if (this.logica.horizontal_izquierda(i, j, this.turno, this.board) == 1) {
                                ImageView posible = new ImageView(contexto);
                                posible.setBackgroundResource(R.drawable.moviento_posible);
                                posible.setMaxHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumWidth(casilla[0][0].getMinWidth());
                                posible.setMaxWidth(casilla[0][0].getMaxWidth());
                                casilla[i][j + 1].addView(posible);
                                board[i][j + 1] = Constantes.POSIBLE;
                            }
                        }
                    }
                    //--------- validando movimiento: vertical_abajo ---------//
                    if (i > 0  && i<Constantes.FILAS-1) {
                        if ((board[i - 1][j] == Constantes.VACIA) && (board[i - 1][0] >= 0) && (board[i + 1][0] < Constantes.FILAS)) {
                            if (this.logica.vertical_abajo(i, j, this.turno, this.board) == 1) {
                                ImageView posible = new ImageView(contexto);
                                posible.setBackgroundResource(R.drawable.moviento_posible);
                                posible.setMaxHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumWidth(casilla[0][0].getMinWidth());
                                posible.setMaxWidth(casilla[0][0].getMaxWidth());
                                casilla[i - 1][j].addView(posible);
                                board[i - 1][j] = Constantes.POSIBLE;
                            }
                        }
                    }
                    //--------- validando movimiento: vertical_arriba ---------//
                    if (i < Constantes.FILAS - 1 && i>0) {
                        if ((board[i + 1][j] == Constantes.VACIA) && (board[i - 1][0] >= 0) && (board[i + 1][0] < Constantes.FILAS)) {
                            if (this.logica.vertical_arriba(i, j, this.turno, this.board) == 1) {
                                ImageView posible = new ImageView(contexto);
                                posible.setBackgroundResource(R.drawable.moviento_posible);
                                posible.setMaxHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumHeight(casilla[0][0].getMaxHeight());
                                posible.setMinimumWidth(casilla[0][0].getMinWidth());
                                posible.setMaxWidth(casilla[0][0].getMaxWidth());
                                casilla[i + 1][j].addView(posible);
                                board[i + 1][j] = Constantes.POSIBLE;
                            }
                        }
                    }
                }

            }
        }
    }

    public void jugada_valida(int y, int x) {
        if(existencia_mov_posibles()) {

            if (board[y][x] == Constantes.POSIBLE) {
                System.out.println("Jugada valida board: "+board[y][x]);
                robar_fichas(y, x,board);
                nBlancas=numero_fblancas();
                nNegras=numero_fnegras();
                textView_blancas.setText(String.valueOf(nBlancas));
                textView_negras.setText(String.valueOf(nNegras));
                if (turno == Constantes.NEGRA) {
                    turno = Constantes.BLANCA;
                    movimiento_posible();
                } else {
                    turno = Constantes.NEGRA;
                    movimiento_posible();
                }
            } else {
                Toast.makeText(juego.getApplicationContext(), "¡JUGADA INVALIDA!", Toast.LENGTH_LONG).show();
                System.out.println("Jugada invalida");
            }
        }else{
            Toast.makeText(juego.getApplicationContext(), "¡Fin del juego!", Toast.LENGTH_LONG).show();
        }
    }
    private boolean existencia_mov_posibles(){
        for (int i = 0; i < Constantes.FILAS; i++) {
            for (int j = 0; j < Constantes.COLUMNAS; j++) {
                if(board[i][j]==Constantes.POSIBLE){
                    return true;
                }
            }
        }
        return false;
    }


    private void robar_fichas(int fila, int columna,int [][] board) {
        int oponente;
        if (turno == Constantes.BLANCA) {
            oponente = Constantes.NEGRA;
        } else oponente = Constantes.BLANCA;

        //---------  movimiento: vertical_arriba ---------//
        System.out.println("ROBAR FICHAS: fila " + fila + " columna " + columna);
        int j = columna;
        if (fila>0){
            if (board[fila - 1][j] == oponente && this.logica.vertical_arriba(fila - 1, j, this.turno, this.board) == 1) {
                for (int y = fila; y >= 0; y--) {
                    //----------TURNO PARA LAS NEGRAS---------//
                    if (turno == Constantes.NEGRA) {
                        if (board[y][j] == Constantes.NEGRA) {
                            break;
                        }
                        actualizar_fichas_robo(y, j);
                    }
                    //----------TURNO PARA LAS BLANCAS---------//
                    else {
                        if (board[y][j] == Constantes.BLANCA) {
                            break;
                        }
                        actualizar_fichas_robo(y, j);
                    }
                }
            }
    }
        //---------  movimiento: vertical_abajo ---------//
        if (fila < Constantes.FILAS-1) {
            if (board[fila + 1][j] == oponente && this.logica.vertical_abajo(fila + 1, j, this.turno, this.board) == 1) {
                for (int y = fila; y < Constantes.FILAS; y++) {
                    //----------TURNO PARA LAS NEGRAS---------//
                    if (turno == Constantes.NEGRA) {
                        if (board[y][j] == Constantes.NEGRA) {
                            break;
                        }
                        actualizar_fichas_robo(y, columna);
                    }
                    //----------TURNO PARA LAS BLANCAS---------//
                    else {
                        if (board[y][j] == Constantes.BLANCA) {
                            break;
                        }
                        actualizar_fichas_robo(y, j);
                    }

                }
            }
        }
        //---------  movimiento: horizontal_izquierda ---------//
        if (columna > 0) {
            if (board[fila][columna - 1] == oponente && this.logica.horizontal_izquierda(fila, columna - 1, this.turno, this.board) == 1) {
                for (int x = columna; x >= 0; x--) {
                    //----------TURNO PARA LAS NEGRAS---------//
                    if (turno == Constantes.NEGRA) {
                        if (board[fila][x] == Constantes.NEGRA) {
                            break;
                        }
                        actualizar_fichas_robo(fila, x);
                    }
                    //----------TURNO PARA LAS BLANCAS---------//
                    else {
                        if (board[fila][x] == Constantes.BLANCA) {
                            break;
                        }
                        actualizar_fichas_robo(fila, x);
                    }
                }
            }
        }
        //---------  movimiento: horizontal_derecha ---------//
        if (columna  < Constantes.COLUMNAS-1) {
            if (board[fila][columna + 1] == oponente && this.logica.horizontal_derecha(fila, columna + 1, this.turno, this.board) == 1) {
                for (int x = columna; x < Constantes.COLUMNAS; x++) {
                    //----------TURNO PARA LAS NEGRAS---------//
                    if (turno == Constantes.NEGRA) {
                        if (board[fila][x] == Constantes.NEGRA) {
                            break;
                        }
                        actualizar_fichas_robo(fila, x);
                    }
                    //----------TURNO PARA LAS BLANCAS---------//
                    else {
                        if (board[fila][x] == Constantes.BLANCA) {
                            break;
                        }
                        actualizar_fichas_robo(fila, x);
                    }
                }
            }
        }
        //---------  movimiento: mov_diagonal_izquierda_abajo ---------//
        if (fila<Constantes.FILAS-1 && columna>0) {
            if (board[fila + 1][columna - 1] == oponente && this.logica.diagonal_izquierda_abajo(fila + 1, columna - 1, this.turno, this.board) == 1) {
                for (int y = fila, x = columna; y < Constantes.FILAS || x >= 0; y++, x--) {
                    //----------TURNO PARA LAS NEGRAS---------//
                    if (turno == Constantes.NEGRA) {
                        if (board[y][x] == Constantes.NEGRA) {
                            break;
                        }
                        actualizar_fichas_robo(y, x);
                    }
                    //----------TURNO PARA LAS BLANCAS---------//
                    else {
                        if (board[y][x] == Constantes.BLANCA) {
                            break;
                        }
                        actualizar_fichas_robo(y, x);
                    }
                }
            }
        }
        //---------  movimiento: mov_diagonal_izquierda_arriba ---------//
        if (fila>0 && columna>0) {
            if (board[fila - 1][columna - 1] == oponente && this.logica.diagonal_izquierda_arriba(fila - 1, columna - 1, this.turno, this.board) == 1) {
                for (int y = fila, x = columna; y >= 0 || x >= 0; y--, x--) {
                    //----------TURNO PARA LAS NEGRAS---------//
                    if (turno == Constantes.NEGRA) {
                        if (board[y][x] == Constantes.NEGRA) {
                            break;
                        }
                        actualizar_fichas_robo(y, x);
                    }
                    //----------TURNO PARA LAS BLANCAS---------//
                    else {
                        if (board[y][x] == Constantes.BLANCA) {
                            break;
                        }
                        actualizar_fichas_robo(y, x);
                    }
                }
            }
        }
        //---------  movimiento: mov_diagonal_derecha_abajo ---------//
        if (fila<Constantes.FILAS-1 && columna<Constantes.COLUMNAS-1) {
            if (board[fila + 1][columna + 1] == oponente && this.logica.diagonal_derecha_abajo(fila + 1, columna + 1, this.turno, this.board) == 1) {
                for (int y = fila, x = columna; y < Constantes.FILAS || x < Constantes.COLUMNAS; y++, x++) {
                    //----------TURNO PARA LAS NEGRAS---------//
                    System.out.println("Fila y: " + y + " colum x: " + x + " Tablero" + board[y][x]);
                    if (turno == Constantes.NEGRA) {
                        if (board[y][x] == Constantes.NEGRA) {
                            break;
                        }
                        actualizar_fichas_robo(y, x);
                    }
                    //----------TURNO PARA LAS BLANCAS---------//
                    else {
                        if (board[y][x] == Constantes.BLANCA && board[y][x] != Constantes.POSIBLE) {
                            break;
                        }
                        actualizar_fichas_robo(y, x);
                    }
                }
            }
        }
        //---------  movimiento: mov_diagonal_derecha_arriba ---------//
        if (fila>0 && columna<Constantes.COLUMNAS-1) {
            if (board[fila - 1][columna + 1] == oponente && this.logica.diagonal_derecha_arriba(fila - 1, columna + 1, this.turno, this.board) == 1) {
                for (int y = fila, x = columna; y >= 0 || x < Constantes.COLUMNAS; y--, x++) {
                    //----------TURNO PARA LAS NEGRAS---------//
                    if (turno == Constantes.NEGRA) {
                        if (board[y][x] == Constantes.NEGRA) {
                            break;
                        }
                        actualizar_fichas_robo(y, x);
                    }
                    //----------TURNO PARA LAS BLANCAS---------//
                    else {
                        if (board[y][x] == Constantes.BLANCA) {
                            break;
                        }
                        actualizar_fichas_robo(y, x);
                    }
                }
            }
        }
        eliminar_posibles();
    }
    private void actualizar_fichas_robo(int i, int j){
        ImageView update_ficha = new ImageView(contexto);
        if(turno==Constantes.NEGRA){
            update_ficha.setBackgroundResource(R.drawable.fazul);
            update_ficha.setMaxHeight(casilla[0][0].getMaxHeight());
            update_ficha.setMinimumHeight(casilla[0][0].getMaxHeight());
            update_ficha.setMinimumWidth(casilla[0][0].getMinWidth());
            update_ficha.setMaxWidth(casilla[0][0].getMaxWidth());
            casilla[i][j].removeAllViews();
            casilla[i][j].addView(update_ficha);
            board[i][j] = Constantes.NEGRA;
        }else{
            update_ficha.setBackgroundResource(R.drawable.fblanca);
            update_ficha.setMaxHeight(casilla[0][0].getMaxHeight());
            update_ficha.setMinimumHeight(casilla[0][0].getMaxHeight());
            update_ficha.setMinimumWidth(casilla[0][0].getMinWidth());
            update_ficha.setMaxWidth(casilla[0][0].getMaxWidth());
            casilla[i][j].removeAllViews();
            casilla[i][j].addView(update_ficha);
            board[i][j] = Constantes.BLANCA;
        }
    }
    public boolean juego_online(){
        for (int i = 0; i < Constantes.FILAS; i++){
            for (int j = 0; j < Constantes.COLUMNAS; j++) {
                if(board[i][j]==Constantes.POSIBLE) {
                    return true;
                }
            }
        }
        return false;
    }
    private void eliminar_posibles(){
        for(int i=0; i<Constantes.FILAS; i++){
            for(int j=0; j<Constantes.COLUMNAS; j++){
                if(board[i][j]==Constantes.POSIBLE){
                    casilla[i][j].removeAllViews();
                    board[i][j]=Constantes.VACIA;
                }
            }
        }
    }
    public int numero_fblancas(){
        int n_blancas=0;
        for(int i=0; i<Constantes.FILAS; i++){
            for(int j=0; j<Constantes.COLUMNAS; j++){
                if(board[i][j]==Constantes.BLANCA){
                    n_blancas++;
                }
            }
        }
        return n_blancas;
    }
    private int numero_fnegras(){
       int  n_negras=0;
        for(int i=0; i<Constantes.FILAS; i++){
            for(int j=0; j<Constantes.COLUMNAS; j++){
                if(board[i][j]==Constantes.NEGRA){
                    n_negras++;
                }
            }
        }
        return n_negras;
}
}
