package com.example.game.modelo;

import com.example.game.util.Constantes;

//jugadas posibles
public class Logica {
    public boolean turno_blancas;
    public boolean turno_negras;
    private Actualizar_juego actualizarjuego;

    public Logica() {
    }
    //reciben coordenadas del oponente

    public int horizontal_derecha(int fila, int columna, int turno, int[][] board) {
        int posible = 0;
        if(columna<=Constantes.COLUMNAS-1) {
            System.out.println("LogicaHD fila "+fila+" columna "+columna);
            for (int x = columna; x <= Constantes.COLUMNAS-1; x++) {
                if (board[fila][x] == Constantes.VACIA || board[fila][x] == Constantes.POSIBLE) {
                    break;
                }

                if (turno == Constantes.NEGRA) {
                    if (board[fila][x] == Constantes.NEGRA) {
                        posible = 1;
                        break;
                    }
                } else if (turno == Constantes.BLANCA) {
                    if (board[fila][x] == Constantes.BLANCA) {
                        posible = 1;
                        break;
                    }
                }
            }
        }
        return posible;
    }

    public int horizontal_izquierda(int fila, int columna, int turno, int[][] board) {
        int posible = 0;
        if(columna>0) {
            System.out.println("LogicaHI fila "+fila+" columna "+columna);
            for (int x = columna; x >= 0; x--) {
                if (board[fila][x] == Constantes.VACIA || board[fila][x] == Constantes.POSIBLE) {
                    break;
                }

                if (turno == Constantes.NEGRA) {
                    if (board[fila][x] == Constantes.NEGRA) {
                        posible = 1;
                        break;
                    }
                } else if (turno == Constantes.BLANCA) {
                    if (board[fila][x] == Constantes.BLANCA) {
                        posible = 1;
                        break;
                    }
                }
            }
        }
        return posible;
    }

    public int vertical_arriba(int fila, int columna, int turno, int[][] board) {
        int posible = 0;
        if(fila>0) {
            System.out.println("LogicaVA fila "+fila+" columna "+columna);
            for (int y = fila; y >= 0; y--) {
                if (board[y][columna] == Constantes.VACIA || board[y][columna] == Constantes.POSIBLE) {
                    break;
                }

                if (turno == Constantes.NEGRA) {
                    if (board[y][columna] == Constantes.NEGRA) {
                        posible = 1;
                        break;
                    }
                } else if (turno == Constantes.BLANCA) {
                    if (board[y][columna] == Constantes.BLANCA) {
                        posible = 1;
                        break;
                    }
                }
            }
        }
        return posible;
    }

    public int vertical_abajo(int fila, int columna, int turno, int[][] board) {
        int posible = 0;
        if(fila<=Constantes.FILAS-1) {
            System.out.println("LogicaVAB fila "+fila+" columna "+columna);
            for (int y = fila; y <= Constantes.FILAS-1; y++) {
                if (board[y][columna] == Constantes.VACIA || board[y][columna] == Constantes.POSIBLE) {
                    break;
                }

                if (turno == Constantes.NEGRA) {
                    if (board[y][columna] == Constantes.NEGRA) {
                        posible = 1;
                        break;
                    }
                } else if (turno == Constantes.BLANCA) {
                    if (board[y][columna] == Constantes.BLANCA) {
                        posible = 1;
                        break;
                    }
                }
            }
        }
        return posible;
    }

    public int diagonal_izquierda_arriba(int fila, int columna, int turno, int[][] board) {
        int posible = 0;
        if(fila>0 && columna>0) {
            System.out.println("LogicaDIA fila "+fila+" columna "+columna);
            for (int y = fila, x = columna; y >= 0 &&  x >= 0; y--, x--) {
                if (board[y][x] == Constantes.VACIA || board[y][x] == Constantes.POSIBLE) {
                    break;
                }

                if (turno == Constantes.NEGRA) {
                    if (board[y][x] == Constantes.NEGRA) {
                        posible = 1;
                        break;
                    }
                } else if (turno == Constantes.BLANCA) {
                    if (board[y][x] == Constantes.BLANCA) {
                        posible = 1;
                        break;
                    }
                }
            }
        }
        return posible;
    }

    public int diagonal_izquierda_abajo(int fila, int columna, int turno, int[][] board) {

        int posible = 0;
        if(fila<=Constantes.FILAS-1 && columna>0) {
            System.out.println("LogicaDIAB fila "+fila+" columna "+columna);
            for (int y = fila, x = columna; y <= Constantes.FILAS-1 &&  x >= 0; y++, x--) {
                if (board[y][x] == Constantes.VACIA || board[y][x] == Constantes.POSIBLE) {
                    break;
                }

                if (turno == Constantes.NEGRA) {
                    if (board[y][x] == Constantes.NEGRA) {
                        posible = 1;
                        break;
                    }
                } else if (turno == Constantes.BLANCA) {
                    if (board[y][x] == Constantes.BLANCA) {
                        posible = 1;
                        break;
                    }
                }
            }
        }
        return posible;
    }

    public int diagonal_derecha_abajo(int fila, int columna, int turno, int[][] board) {
        int posible = 0;
        if(fila<=Constantes.FILAS-1 && columna<=Constantes.COLUMNAS-1) {
            System.out.println("LogicaDDAB fila "+fila+" columna "+columna);
            for (int y = fila, x = columna; y <= Constantes.FILAS-1 && x <= Constantes.COLUMNAS-1; y++, x++) {
                if (board[y][x] == Constantes.VACIA || board[y][x] == Constantes.POSIBLE) {
                    break;
                }

                if (turno == Constantes.NEGRA) {
                    if (board[y][x] == Constantes.NEGRA) {
                        posible = 1;
                        break;
                    }
                } else if (turno == Constantes.BLANCA) {
                    if (board[y][x] == Constantes.BLANCA) {
                        posible = 1;
                        break;
                    }
                }
            }
        }
        return posible;
    }

    public int diagonal_derecha_arriba(int fila, int columna, int turno, int[][] board) {
        int posible = 0;
        if(fila>0 && columna<=Constantes.COLUMNAS-1) {
            System.out.println("Logica filaDBA "+fila+" columna "+columna);
            for (int y = fila, x = columna; y >= 0 && x <= Constantes.COLUMNAS-1; y--, x++) {
                int a=Constantes.COLUMNAS-1;
                System.out.println("Constantes.COLUMNAS-1 "+a);
                System.out.println("Logica filaDBA ITERACION "+y+" columna "+x);
                if (board[y][x] == Constantes.VACIA || board[y][x] == Constantes.POSIBLE) {
                    break;
                }

                if (turno == Constantes.NEGRA) {
                    if (board[y][x] == Constantes.NEGRA) {
                        posible = 1;
                        break;
                    }
                } else if (turno == Constantes.BLANCA) {
                    if (board[y][x] == Constantes.BLANCA) {
                        posible = 1;
                        break;
                    }
                }
            }
        }
        return posible;
    }

}
