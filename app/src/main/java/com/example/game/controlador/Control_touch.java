package com.example.game.controlador;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.game.modelo.Coordenadas;
import com.example.game.util.Constantes;

import java.util.ArrayList;
import java.util.Observable;
public class Control_touch  extends Observable{

       private ConstraintLayout casillas[][];

        public Control_touch(ConstraintLayout casillas[][]){
            this.casillas=casillas;
            for (int i = 0; i < Constantes.FILAS; i++){
                for (int j = 0; j < Constantes.COLUMNAS; j++){
                     final int posicioni = i;
                     final int posicionj = j;
                    casillas[i][j].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            click(posicioni,posicionj);
                        }
                    });
                }
            }

        }
        public void click(int i, int j){
            Coordenadas cordenadas= new Coordenadas(i,j);
            ArrayList<Object> args= new ArrayList<Object>();
            args.add(cordenadas);
            this.setChanged();
            this.notifyObservers(args);
        }

    }

